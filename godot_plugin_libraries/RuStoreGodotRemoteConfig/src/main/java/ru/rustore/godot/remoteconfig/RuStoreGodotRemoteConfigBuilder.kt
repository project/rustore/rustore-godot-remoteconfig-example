package ru.rustore.godot.remoteconfig

import android.content.Context
import ru.rustore.godot.remoteconfig.model.GodotRemoteConfigClientEventListener
import ru.rustore.godot.remoteconfig.model.GodotUpdateBehaviour
import ru.rustore.godot.remoteconfig.model.RemoteConfigParameters
import ru.rustore.sdk.remoteconfig.AppBuild
import ru.rustore.sdk.remoteconfig.AppId
import ru.rustore.sdk.remoteconfig.AppVersion
import ru.rustore.sdk.remoteconfig.DeviceId
import ru.rustore.sdk.remoteconfig.DeviceModel
import ru.rustore.sdk.remoteconfig.Environment
import ru.rustore.sdk.remoteconfig.OsVersion
import ru.rustore.sdk.remoteconfig.RemoteConfigClientBuilder
import ru.rustore.sdk.remoteconfig.UpdateBehaviour
import kotlin.time.Duration

object RuStoreGodotRemoteConfigBuilder {
    private var isInitialized: Boolean = false

    fun init(appId: String,
             updateBehaviour: GodotUpdateBehaviour,
             updateTime: Int,
             parameters: RemoteConfigParameters?,
             eventListener: GodotRemoteConfigClientEventListener?,
             applicationContext: Context)
    {
        if (isInitialized) return

        val builder = RemoteConfigClientBuilder(
            appId = AppId(appId),
            context = applicationContext
        )

        eventListener?.let{
            RemoteConfigClientEventListenerDefault.setListener(eventListener)
        }
        builder.setRemoteConfigClientEventListener(RemoteConfigClientEventListenerDefault)
        builder.setConfigRequestParameterProvider(ConfigRequestParameterProviderDefault)

        parameters?.apply {
            appBuild?.let { builder.setAppBuild(AppBuild(it)) }
            appVersion?.let { builder.setAppVersion(AppVersion(it)) }
            deviceId?.let { builder.setDeviceId(DeviceId(it)) }
            deviceModel?.let { builder.setDevice(DeviceModel(it)) }
            environment?.let { builder.setEnvironment(Environment.valueOf(it)) }
            osVersion?.let { builder.setOsVersion(OsVersion(it)) }
        }

        val behaviour: UpdateBehaviour = when (updateBehaviour) {
            GodotUpdateBehaviour.Actual -> UpdateBehaviour.Actual
            GodotUpdateBehaviour.Snapshot -> UpdateBehaviour.Snapshot(Duration.parse(updateTime.toString() + "m"))
            GodotUpdateBehaviour.Default -> UpdateBehaviour.Default(Duration.parse(updateTime.toString() + "m"))
        }

        builder.setUpdateBehaviour(behaviour).build().init()

        isInitialized = true
    }

    fun setAccount(value: String) {
        ConfigRequestParameterProviderDefault.setAccount(value)
    }

    fun getAccount(): String = ConfigRequestParameterProviderDefault.getAccount()

    fun setLanguage(value: String) {
        ConfigRequestParameterProviderDefault.setLanguage(value)
    }

    fun getLanguage(): String = ConfigRequestParameterProviderDefault.getLanguage()
}
