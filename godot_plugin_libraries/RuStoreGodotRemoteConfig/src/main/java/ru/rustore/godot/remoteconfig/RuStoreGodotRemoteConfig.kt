package ru.rustore.godot.remoteconfig

import android.net.Uri
import android.util.ArraySet
import android.util.Log
import com.google.gson.GsonBuilder
import org.godotengine.godot.Godot
import org.godotengine.godot.plugin.GodotPlugin
import org.godotengine.godot.plugin.SignalInfo
import org.godotengine.godot.plugin.UsedByGodot
import ru.rustore.godot.core.JsonBuilder
import ru.rustore.godot.core.UriTypeAdapter
import ru.rustore.godot.remoteconfig.model.GodotRemoteConfigClientEventListener
import ru.rustore.sdk.remoteconfig.RemoteConfigClient

class RuStoreGodotRemoteConfig(godot: Godot?): GodotPlugin(godot), GodotRemoteConfigClientEventListener {
	private companion object {
		const val PLUGIN_NAME = "RuStoreGodotRemoteConfig"
		const val CHANNEL_GET_REMOTE_CONFIG_SUCCESS = "rustore_get_remote_config_success"
		const val CHANNEL_GET_REMOTE_CONFIG_FAILURE = "rustore_get_remote_config_failure"
		const val CHANNEL_BACKGROUND_JOB_ERRORS = "rustore_background_job_errors"
		const val CHANNEL_FIRST_LOAD_COMPLETE = "rustore_first_load_complete"
		const val CHANNEL_INIT_COMPLETE = "rustore_init_complete"
		const val CHANNEL_MEMORY_CACHE_UPDATED = "rustore_memory_cache_updated"
		const val CHANNEL_PERSISTENT_STORAGE_UPDATED = "rustore_persistent_storage_updated"
		const val CHANNEL_REMOTE_CONFIG_NETWORK_REQUEST_FAILURE = "rustore_remote_config_network_request_failure"
	}

	override fun getPluginName(): String {
		return PLUGIN_NAME
	}

	override fun getPluginSignals(): Set<SignalInfo> {
		val signals: MutableSet<SignalInfo> = ArraySet()
		signals.add(SignalInfo(CHANNEL_GET_REMOTE_CONFIG_SUCCESS, String::class.java))
		signals.add(SignalInfo(CHANNEL_GET_REMOTE_CONFIG_FAILURE, String::class.java))
		signals.add(SignalInfo(CHANNEL_BACKGROUND_JOB_ERRORS, String::class.java))
		signals.add(SignalInfo(CHANNEL_FIRST_LOAD_COMPLETE))
		signals.add(SignalInfo(CHANNEL_INIT_COMPLETE))
		signals.add(SignalInfo(CHANNEL_MEMORY_CACHE_UPDATED))
		signals.add(SignalInfo(CHANNEL_PERSISTENT_STORAGE_UPDATED))
		signals.add(SignalInfo(CHANNEL_REMOTE_CONFIG_NETWORK_REQUEST_FAILURE, String::class.java))

		return signals
	}

	private val gson = GsonBuilder()
		.registerTypeAdapter(Uri::class.java, UriTypeAdapter())
		.create()

	@UsedByGodot
	fun initEventListener() {
		RemoteConfigClientEventListenerDefault.setListener(this)
	}


	@UsedByGodot
	fun getRemoteConfig() {
		RemoteConfigClient.instance.getRemoteConfig()
			.addOnSuccessListener { result ->
				emitSignal(CHANNEL_GET_REMOTE_CONFIG_SUCCESS, gson.toJson(result))
			}
			.addOnFailureListener { throwable ->
				emitSignal(CHANNEL_GET_REMOTE_CONFIG_FAILURE, JsonBuilder.toJson(throwable))
				Log.w("ssss", JsonBuilder.toJson(throwable.cause))
			}
	}

	@UsedByGodot
	fun setAccount(value: String) {
		ConfigRequestParameterProviderDefault.setAccount(value)
	}

	@UsedByGodot
	fun getAccount(): String = ConfigRequestParameterProviderDefault.getAccount()

	@UsedByGodot
	fun setLanguage(value: String) {
		ConfigRequestParameterProviderDefault.setLanguage(value)
	}

	@UsedByGodot
	fun getLanguage(): String = ConfigRequestParameterProviderDefault.getLanguage()


	override fun backgroundJobErrors(exception: Throwable) =
		emitSignal(CHANNEL_BACKGROUND_JOB_ERRORS, JsonBuilder.toJson(exception))
	override fun firstLoadComplete() = emitSignal(CHANNEL_FIRST_LOAD_COMPLETE)
	override fun initComplete() = emitSignal(CHANNEL_INIT_COMPLETE)
	override fun memoryCacheUpdated() = emitSignal(CHANNEL_MEMORY_CACHE_UPDATED)
	override fun persistentStorageUpdated() = emitSignal(CHANNEL_PERSISTENT_STORAGE_UPDATED)
	override fun remoteConfigNetworkRequestFailure(throwable: Throwable) =
		emitSignal(CHANNEL_REMOTE_CONFIG_NETWORK_REQUEST_FAILURE, JsonBuilder.toJson(throwable))
}
