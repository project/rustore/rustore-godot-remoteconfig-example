package ru.rustore.godot.remoteconfig

import ru.rustore.sdk.remoteconfig.Account
import ru.rustore.sdk.remoteconfig.ConfigRequestParameter
import ru.rustore.sdk.remoteconfig.ConfigRequestParameterProvider
import ru.rustore.sdk.remoteconfig.Language

object ConfigRequestParameterProviderDefault : ConfigRequestParameterProvider {
    private var account: Account? = null
    private var language: Language? = null

    fun setAccount(value: String) {
        account = Account(value)
    }

    fun getAccount(): String {
        return account?.value.orEmpty()
    }

    fun setLanguage(value: String) {
        language = Language(value)
    }

    fun getLanguage(): String {
        return language?.value.orEmpty()
    }

    override fun getConfigRequestParameter(): ConfigRequestParameter {
        return ConfigRequestParameter(
            language = language,
            account = account
        )
    }
}
