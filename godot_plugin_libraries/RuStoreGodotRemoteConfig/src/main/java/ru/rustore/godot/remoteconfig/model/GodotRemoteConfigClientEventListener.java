package ru.rustore.godot.remoteconfig.model;

public interface GodotRemoteConfigClientEventListener {
    void backgroundJobErrors(Throwable exception);
    void firstLoadComplete();
    void initComplete();
    void memoryCacheUpdated();
    void persistentStorageUpdated();
    void remoteConfigNetworkRequestFailure(Throwable throwable);
}
