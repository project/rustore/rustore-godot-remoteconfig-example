package ru.rustore.godot.remoteconfig.model;

public enum GodotUpdateBehaviour {
    Actual,
    Default,
    Snapshot
}
