package ru.rustore.godot.remoteconfig.model

import ru.rustore.sdk.remoteconfig.RemoteConfigException

sealed class RemoteConfigClientEventListenerEventItem {
    data class BackgroundJobErrors(val exception: RemoteConfigException.BackgroundConfigUpdateError) : RemoteConfigClientEventListenerEventItem()
    object FirstLoadComplete : RemoteConfigClientEventListenerEventItem()
    object InitComplete : RemoteConfigClientEventListenerEventItem()
    object MemoryCacheUpdated : RemoteConfigClientEventListenerEventItem()
    object PersistentStorageUpdated : RemoteConfigClientEventListenerEventItem()
    data class RemoteConfigNetworkRequestFailure(val throwable: Throwable) : RemoteConfigClientEventListenerEventItem()
}
