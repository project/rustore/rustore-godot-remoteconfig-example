## История изменений

### Release 8.0.0
- Версия SDK remoteconfig 8.0.0.

### Release 7.0.0
- Версия SDK remoteconfig 7.0.0.

### Release 6.1.0
- Версия SDK remoteconfig 6.1.0.

### Release 6.0.0
- Версия SDK remoteconfig 6.+.

### Release 1.0
- Версия SDK remoteconfig 1.+.
- Поддержка Godot 4.2+.

### Release 0.0.2
- Версия SDK remoteconfig 0.0.2.
