## RuStore Godot плагин для работы с облачным сервисом конфигурации приложения

### [🔗 Документация разработчика][10]

SDK Remote Config это облачный сервис, который позволяет изменять поведение и внешний вид вашего приложения, не требуя от пользователей загрузки обновления приложения. SDK инкапсулирует в себе запрос конфигурации с сервера, кэширование, фоновое обновление.

Репозиторий содержит плагины “RuStoreGodotRemoteConfig” и “RuStoreGodotCore”, а также демонстрационное приложение с примерами использования и настроек. Поддерживаются версии Godot 4.2.1+.


### Сборка примера приложения

Вы можете ознакомиться с демонстрационным приложением содержащим представление работы всех методов sdk:
- [README](godot_example/README.md)
- [godot_example](https://gitflic.ru/project/rustore/godot-rustore-billing/file?file=godot_example)


### Установка плагина в свой проект

> ⚠️ Библиотеки плагинов в репозитории собраны для Godot Engine 4.2.1. Если вы используете другую версию Godot Engine, выполните шаги раздела _"Пересборка плагина"_.

1. Скопируйте содержимое папки _“godot_example / android / plugins”_ в папку _“*your_project* / android / plugins”_.

2. В пресете сборки Android в списке "Плагины" отметьте плагины “Ru Store Godot Remote Config” и “Ru Store Godot Core”


### Пересборка плагина

Если вам необходимо изменить код библиотек плагинов, вы можете внести изменения и пересобрать подключаемые .aar файлы.

1. Откройте в вашей IDE проект Android из папки _“godot_plugin_libraries”_.

2. Поместите в папку _“godot_plugin_libraries / libs”_ пакет _“godot-lib.xxx.yyy.template_release.aar”_, где _xxx.yyy_ версия вашей редакции Godot Engine.

3. Выполните сборку проекта командой gradle assemble.

При успешном выполнении сборки в папке _“godot_example / android / plugins”_ будут обновлены файлы:
- RuStoreGodotRemoteConfig.gdap
- RuStoreGodotRemoteConfig.aar
- RuStoreGodotCore.gdap
- RuStoreGodotCore.aar


### История изменений

[CHANGELOG](CHANGELOG.md)


### Условия распространения

Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы распространяется под лицензией MIT. Информация о лицензировании доступна в документе [MIT-LICENSE](MIT-LICENSE.txt).


### Техническая поддержка

Дополнительная помощь и инструкции доступны на странице [rustore.ru/help/](https://www.rustore.ru/help/) и по электронной почте [support@rustore.ru](mailto:support@rustore.ru).

[10]: https://www.rustore.ru/help/developers/tools/remote-config/sdk/godot/8-0-0
