# RuStoreGodotRemoteConfigClient
# @brief
#	Класс реализует API для получения конфигурации данных с удаленного сервера.
#	Инкапсулирует запрос конфигурации с сервера, кэширование, фоновое обновление.
class_name RuStoreGodotRemoteConfigClient extends Object

const SINGLETON_NAME = "RuStoreGodotRemoteConfig"

# @brief Действие, выполняемое при успешном завершении get_remote_config.
signal on_get_remote_config_success

# @brief Действие, выполняемое в случае ошибки get_remote_config.
signal on_get_remote_config_failure

# @brief Возвращает ошибку фоновой работы.
signal on_rustore_background_job_errors

# @brief Вызывается при окончании первой загрузки.
signal on_rustore_first_load_complete

# @brief Вызывается при окончании инциализации.
signal on_rustore_init_complete

# @brief Вызывается при изменениях кэша в памяти.
signal on_rustore_memory_cache_updated

# @brief Вызывается при изменении постоянного хранилища.
signal on_rustore_persistent_storage_updated

# @brief Вызывается при ошибке сетевого запроса Remote Сonfig.
signal on_rustore_remote_config_network_request_failure

# @brief Возвращает true, если синглтон инициализирован, в противном случае — false.
var isInitialized: bool = false
var _clientWrapper: Object = null

static var _instance: RuStoreGodotRemoteConfigClient = null


# @brief
#	Получить экземпляр RuStoreGodotRemoteConfigClient.
# @return
#	Возвращает указатель на единственный экземпляр RuStoreGodotRemoteConfigClient (реализация паттерна Singleton).
#	Если экземпляр еще не создан, создает его.
static func get_instance() -> RuStoreGodotRemoteConfigClient:
	if _instance == null:
		_instance = RuStoreGodotRemoteConfigClient.new()
	return _instance


func _init():
	if Engine.has_singleton(SINGLETON_NAME):
		_clientWrapper = Engine.get_singleton(SINGLETON_NAME)
		_clientWrapper.rustore_get_remote_config_success.connect(_on_get_remote_config_success)
		_clientWrapper.rustore_get_remote_config_failure.connect(_on_get_remote_config_failure)
		_clientWrapper.rustore_background_job_errors.connect(_on_background_job_errors)
		_clientWrapper.rustore_first_load_complete.connect(_on_first_load_complete)
		_clientWrapper.rustore_init_complete.connect(_on_init_complete)
		_clientWrapper.rustore_memory_cache_updated.connect(_on_memory_cache_updated)
		_clientWrapper.rustore_persistent_storage_updated.connect(_on_persistent_storage_updated)
		_clientWrapper.rustore_remote_config_network_request_failure.connect(_on_remote_config_network_request_failure)
		_clientWrapper.initEventListener()
		isInitialized = true


# @brief Устанавливает параметр Account, который может быть использован для получения заданной конфигурации.
# @param value Значение параметра Account.
func setAccount(value: String):
	_clientWrapper.setAccount(value)


# @brief Получает текущий установленный параметр Account.
# @return Значение параметра Account.
func get_account() -> String:
	return _clientWrapper.getAccount()


# @brief Устанавливает параметр Language, который может быть использован для получения заданной конфигурации.
# @param value Значение параметра Language.
func setLanguage(value: String):
	_clientWrapper.setLanguage(value)


# @brief Получает текущий установленный параметр Language.
# @return Значение параметра Language.
func get_language() -> String:
	return _clientWrapper.getLanguage()


# @brief Получение конфигурации данных в зависимости от выбранной политики обновления при инициализации.
func get_remote_config():
	_clientWrapper.getRemoteConfig()


func _on_get_remote_config_success(json: String):
	var data: Dictionary = JSON.parse_string(json)["data"]
	on_get_remote_config_success.emit(data)


func _on_get_remote_config_failure(json: String):
	var data = RuStoreError.new(json)
	on_get_remote_config_failure.emit(data)


func _on_background_job_errors(json: String):
	var error = RuStoreError.new(json)
	on_rustore_background_job_errors.emit(error)


func _on_first_load_complete():
	on_rustore_first_load_complete.emit()


func _on_init_complete():
	on_rustore_init_complete.emit()


func _on_memory_cache_updated():
	on_rustore_memory_cache_updated.emit()


func _on_persistent_storage_updated():
	on_rustore_persistent_storage_updated.emit()


func _on_remote_config_network_request_failure(json: String):
	var error = RuStoreError.new(json)
	on_rustore_remote_config_network_request_failure.emit(error)
