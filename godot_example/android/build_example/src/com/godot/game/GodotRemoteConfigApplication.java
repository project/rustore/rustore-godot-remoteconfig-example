package com.godot.game;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import ru.rustore.godot.remoteconfig.model.GodotUpdateBehaviour;
import ru.rustore.godot.remoteconfig.RuStoreGodotRemoteConfigBuilder;

public class GodotRemoteConfigApplication extends Application {
	public final String APP_ID = "564e9667-c1f9-4d76-8872-258bc1f14844";
	public final GodotUpdateBehaviour UPDATE_BEHAVIOUR = GodotUpdateBehaviour.Actual;
	public final int UPDATE_TIME = 15;
	public final String ACCOUNT = "Acc1";
	public final String LANGUAGE = "ru";

	@Override
	public void onCreate() {
		super.onCreate();
		
		SharedPreferences preferences = getSharedPreferences("rustore_shared_preferences", Context.MODE_PRIVATE);

		int updateTime = preferences.getInt("rustore_update_time", UPDATE_TIME);
		String updateBehaviourName = preferences.getString("rustore_update_behaviour", "Actual");
		GodotUpdateBehaviour updateBehaviour = GodotUpdateBehaviour.valueOf(updateBehaviourName);
		String account = preferences.getString("rustore_account", ACCOUNT);
		String language = preferences.getString("rustore_language", LANGUAGE);

		RuStoreGodotRemoteConfigBuilder.INSTANCE.setAccount(account);
		RuStoreGodotRemoteConfigBuilder.INSTANCE.setLanguage(language);
		RuStoreGodotRemoteConfigBuilder.INSTANCE.init(APP_ID, updateBehaviour, updateTime, null, null, getApplicationContext());
	}
}
