extends Node

const SHARED_PREFERENCES = "rustore_shared_preferences"

@onready var _log = $VBoxContainer/HBoxContainer/Log
@onready var _accountTextEdit = $VBoxContainer/AccountHBoxContainer/AccountTextEdit
@onready var _languageTextEdit = $VBoxContainer/LanguageHBoxContainer/LanguageTextEdit
@onready var _loading = $LoadingPanel

var _core_client: RuStoreGodotCoreUtils = null
var _remoteConfig_client: RuStoreGodotRemoteConfigClient = null

# Called when the node enters the scene tree for the first time.
func _ready():
	_core_client = RuStoreGodotCoreUtils.get_instance()
	_accountTextEdit.text = _core_client.get_string_shared_preferences(SHARED_PREFERENCES, "rustore_account", "Auf")
	_languageTextEdit.text = _core_client.get_string_shared_preferences(SHARED_PREFERENCES, "rustore_language", "ru")
	
	_remoteConfig_client = RuStoreGodotRemoteConfigClient.get_instance()
	if !_remoteConfig_client.on_get_remote_config_success.is_connected(_on_get_remote_config_success):
		_remoteConfig_client.on_get_remote_config_success.connect(_on_get_remote_config_success)
		_remoteConfig_client.on_get_remote_config_failure.connect(_on_get_remote_config_failure)
		_remoteConfig_client.on_rustore_background_job_errors.connect(_on_rustore_background_job_errors)
		_remoteConfig_client.on_rustore_first_load_complete.connect(_on_rustore_first_load_complete)
		_remoteConfig_client.on_rustore_init_complete.connect(_on_rustore_init_complete)
		_remoteConfig_client.on_rustore_memory_cache_updated.connect(_on_rustore_memory_cache_updated)
		_remoteConfig_client.on_rustore_persistent_storage_updated.connect(_on_rustore_persistent_storage_updated)
		_remoteConfig_client.on_rustore_remote_config_network_request_failure.connect(_on_rustore_remote_config_network_request_failure)
		_log.add_line("Connect signals")


func _on_get_remote_config_success(data: Dictionary):
	_loading.visible = false
	_log.add_line("=== Remote config data ===")
	for key in data.keys():
		var value = data.get(key)
		_log.add_line(key + ": " + value)
	_log.add_line("=== End ===")


func _on_get_remote_config_failure(error: RuStoreError):
	_loading.visible = false
	_log.add_line(error.description)


func _on_rustore_background_job_errors(error: RuStoreError):
	_log.add_line(error.description)


func _on_rustore_first_load_complete():
	_log.add_line("First load complete")


func _on_rustore_init_complete():
	_log.add_line("Init complete")


func _on_rustore_memory_cache_updated():
	_log.add_line("Memory cache updated")


func _on_rustore_persistent_storage_updated():
	_log.add_line("Persistent storage updated")


func _on_rustore_remote_config_network_request_failure(error: RuStoreError):
	_log.add_line(error.description)


func _on_set_request_parameters_button_pressed():
	_core_client.set_string_shared_preferences(SHARED_PREFERENCES, "rustore_account", _accountTextEdit.text)
	_core_client.set_string_shared_preferences(SHARED_PREFERENCES, "rustore_language", _languageTextEdit.text)
	_remoteConfig_client.setAccount(_accountTextEdit.text)
	_remoteConfig_client.setLanguage(_languageTextEdit.text)
	_core_client.show_toast("Request parameters are set")


func _on_get_remote_config_button_pressed():
	_loading.visible = true
	_remoteConfig_client.get_remote_config()


func _on_clear_log_pressed():
	_log.clear_log()
