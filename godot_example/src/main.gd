extends Node

const SHARED_PREFERENCES = "rustore_shared_preferences"
const SECOND_SCENE = "res://scenes/second.tscn"

var _core_client: RuStoreGodotCoreUtils = null

@onready var actualButton: Button = $VBoxContainer/InitRemoteConfigActual
@onready var defaultButton: Button = $VBoxContainer/InitRemoteConfigDefault
@onready var snapshotButton: Button = $VBoxContainer/InitRemoteConfigSnapshot
@onready var continueButton: Button = $VBoxContainer/ContinueButton
@onready var timeTextEdit: TextEdit = $VBoxContainer/TimeHBoxContainer/TimeTextEdit

# Called when the node enters the scene tree for the first time.
func _ready():
	_core_client = RuStoreGodotCoreUtils.get_instance()
	timeTextEdit.text = str(_core_client.get_int_shared_preferences(SHARED_PREFERENCES, "rustore_update_time", 15))
	
	var updateBehaviour = _core_client.get_string_shared_preferences(SHARED_PREFERENCES, "rustore_update_behaviour", "Actual")
	if updateBehaviour == "Actual":
		actualButton.text += "(selected) "
	elif updateBehaviour == "Default":
		defaultButton.text += "(selected) "
	elif updateBehaviour == "Snapshot":
		snapshotButton.text += "(selected) "


func _on_continue_button_pressed():
	get_tree().change_scene_to_file(SECOND_SCENE)


func _on_init_remote_config_actual_pressed():
	continueButton.disabled = true
	_core_client.set_string_shared_preferences(SHARED_PREFERENCES, "rustore_update_behaviour", "Actual")
	_core_client.set_int_shared_preferences(SHARED_PREFERENCES, "rustore_update_time", int(timeTextEdit.text))
	OS.alert("Restart application", "Actual mode set")


func _on_init_remote_config_default_pressed():
	continueButton.disabled = true
	_core_client.set_string_shared_preferences(SHARED_PREFERENCES, "rustore_update_behaviour", "Default")
	_core_client.set_int_shared_preferences(SHARED_PREFERENCES, "rustore_update_time", int(timeTextEdit.text))
	OS.alert("Restart application", "Default mode set")


func _on_init_remote_config_snapshot_pressed():
	continueButton.disabled = true
	_core_client.set_string_shared_preferences(SHARED_PREFERENCES, "rustore_update_behaviour", "Snapshot")
	_core_client.set_int_shared_preferences(SHARED_PREFERENCES, "rustore_update_time", int(timeTextEdit.text))
	OS.alert("Restart application", "Snapshot mode set")
