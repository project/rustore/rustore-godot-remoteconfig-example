extends Node

@onready var _log = $"."

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func clear_log():
	_log.clear()


func add_line(value: String):
	_log.append_text(value)
	_log.newline()
